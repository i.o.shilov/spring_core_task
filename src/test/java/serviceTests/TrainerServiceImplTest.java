package serviceTests;

import org.example.DAO.DAOGenericImpl;
import org.example.domain.Trainer;
import org.example.domain.User;
import org.example.service.TrainerServiceImpl;
import org.example.service.UserServiceImpl;
import org.example.domain.TrainingType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class TrainerServiceImplTest {

    private TrainerServiceImpl trainerService;

    @Mock
    private DAOGenericImpl<Trainer> trainerDAOMock;

    @Mock
    private UserServiceImpl userServiceImplMock;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        trainerService = new TrainerServiceImpl(trainerDAOMock, userServiceImplMock);
    }

    @Test
    public void testGetAllTrainers() {
        List<Trainer> trainers = new ArrayList<>();
        when(trainerDAOMock.getAllObjects()).thenReturn(trainers);

        List<Trainer> result = trainerService.getAllTrainers();

        assertEquals(trainers, result);
        verify(trainerDAOMock, times(1)).getAllObjects();
    }

    @Test
    public void testGetAllObjects() {
        List<Trainer> trainers = new ArrayList<>();
        when(trainerDAOMock.getAllObjects()).thenReturn(trainers);

        List<Trainer> result = trainerService.getAllObjects();

        assertEquals(trainers, result);
        verify(trainerDAOMock, times(1)).getAllObjects();
    }

    @Test
    public void testGetById() {
        int trainerId = 1;
        Trainer trainer = new Trainer(0,0);
        when(trainerDAOMock.getById(trainerId)).thenReturn(trainer);

        Trainer result = trainerService.getById(trainerId);

        assertEquals(trainer, result);
        verify(trainerDAOMock, times(1)).getById(trainerId);
    }

    @Test
    public void testCreateTrainer() {
        String firstName = "John";
        String lastName = "Doe";
        TrainingType trainingType = new TrainingType(1, "Type A");
        when(userServiceImplMock.create(firstName, lastName, true)).thenReturn(new User( firstName, lastName, "johndoe", "password123", true));

        Trainer trainer = trainerService.create(firstName, lastName, true, trainingType);

        assertEquals(trainingType.getId(), trainer.getTrainingTypeId());
        assertEquals(0, trainer.getUserId()); // Assuming userServiceMock.create returns a User with ID 1
        verify(userServiceImplMock, times(1)).create(firstName, lastName, true);
        verify(trainerDAOMock, times(1)).save(trainer);
    }

    @Test
    public void testSave() {
        Trainer trainer = new Trainer(0,0);
        when(trainerDAOMock.getLastId()).thenReturn(0);

        trainerService.save(trainer);

        assertEquals(0, trainer.getId());
        verify(trainerDAOMock, times(1)).getLastId();
        verify(trainerDAOMock, times(1)).save(trainer);
    }


}
