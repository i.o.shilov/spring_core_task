package serviceTests;

import org.example.DAO.DAOGenericImpl;
import org.example.domain.Trainee;
import org.example.domain.User;
import org.example.service.TraineeServiceImpl;
import org.example.service.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TraineeServiceImplTest {

    private TraineeServiceImpl traineeService;

    @Mock
    private DAOGenericImpl<Trainee> traineeDAOMock;

    @Mock
    private UserServiceImpl userServiceImplMock;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        traineeService = new TraineeServiceImpl(traineeDAOMock, userServiceImplMock);
    }

    @Test
    public void testSave() {
        Trainee trainee=new Trainee(LocalDate.of(1914, 7, 28),"homeaddress",0);
        when(traineeDAOMock.getLastId()).thenReturn(1);
        when(traineeDAOMock.save(trainee)).thenReturn(trainee);

        Trainee savedTrainee = traineeService.save(trainee);

        assertEquals(1, savedTrainee.getId());
        verify(traineeDAOMock, times(1)).getLastId();
        verify(traineeDAOMock, times(1)).save(trainee);
    }

    @Test
    public void testCreate() {
        when(userServiceImplMock.getAllObjects()).thenReturn(new ArrayList<>());
        User newUser = new User("John", "Doe", "johndoe", "password123", true);
        when(userServiceImplMock.save(any())).thenReturn(newUser);

        Trainee trainee = traineeService.create("John", "Doe", LocalDate.of(1990, 1, 1), "123 Main St", true);

        assertNotNull(trainee);
        verify(userServiceImplMock, times(1)).save(any());
        verify(traineeDAOMock, times(1)).save(any());
    }

    @Test
    public void testGetAllObjects() {
        List<Trainee> trainees = new ArrayList<>();
        when(traineeDAOMock.getAllObjects()).thenReturn(trainees);

        List<Trainee> result = traineeService.getAllObjects();

        assertEquals(trainees, result);
        verify(traineeDAOMock, times(1)).getAllObjects();
    }

    @Test
    public void testUpdate() {
        Trainee oldTrainee = new Trainee(LocalDate.of(1914, 7, 28),"homeaddress",0);
        Trainee newTrainee = new Trainee(LocalDate.of(2000, 5, 15),"homeaddressReplaced",0);
        when(traineeDAOMock.update(oldTrainee, newTrainee)).thenReturn(true);

        boolean result = traineeService.update(oldTrainee, newTrainee);

        assertTrue(result);
        verify(traineeDAOMock, times(1)).update(oldTrainee, newTrainee);
    }

    @Test
    public void testGetById() {
        int traineeId = 1;
        Trainee trainee=new Trainee(LocalDate.of(1914, 7, 28),"homeaddress",0);
        when(traineeDAOMock.getById(traineeId)).thenReturn(trainee);

        Trainee result = traineeService.getById(traineeId);

        assertEquals(trainee, result);
        verify(traineeDAOMock, times(1)).getById(traineeId);
    }

    @Test
    public void testDelete() {
        int traineeId = 1;
        when(traineeDAOMock.deleteById(traineeId)).thenReturn(true);

        boolean result = traineeService.delete(traineeId);

        assertTrue(result);
        verify(traineeDAOMock, times(1)).deleteById(traineeId);
    }

    @Test
    public void passwordGeneratorTest(){
        String testString=traineeService.passwordGenerator();
        assertEquals(10,testString.length());
    }

    @Test
    public void serialGeneratorTest(){
        int testInt=traineeService.serialGenerator();
        assertTrue(testInt >= 100 && testInt <= 999);
    }
}

