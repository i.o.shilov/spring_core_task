package org.example.DAO;

import jakarta.annotation.PostConstruct;
import org.example.DAO.interfaces.DAO_generic;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DAOGenericImpl<T> implements DAO_generic<T> {

    private int lastId = 0;
    private Map<Integer, T> objectsMap = new HashMap<>();


    @Override
    public T save(T obj) {
        if (!objectsMap.containsValue(obj)) {
            objectsMap.put(lastId, obj);
            lastId++;
        }
        return objectsMap.get(lastId - 1);
    }

    @Override
    public boolean update(T obj, T replace) {
        Integer key = null;
        var mapEntry = objectsMap.entrySet()
                .stream()
                .filter(entry -> obj.equals(entry.getValue()))
                .findFirst();
        if (mapEntry.isPresent()) {
            key = mapEntry.get().getKey();
        }
        objectsMap.remove(key);
        objectsMap.put(key, replace);
        return true;
    }

    @Override
    public boolean deleteById(int id) {
        if (objectsMap.containsKey(id)) {
            objectsMap.remove(id);
            return true;
        }
        return false;
    }

    @Override
    public T getById(int id) {
        return objectsMap.get(id);
    }

    @Override
    public List<T> getAllObjects() {
        return new ArrayList<>(objectsMap.values());
    }

    public int getLastId() {
        return lastId;
    }

    public void setObjectsMap(Map<Integer, T> objectsMap) {
        this.objectsMap = objectsMap;
    }

    @PostConstruct
    public void update() {
        lastId = objectsMap.size();
    }
}
