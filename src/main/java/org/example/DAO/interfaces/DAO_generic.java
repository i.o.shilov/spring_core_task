package org.example.DAO.interfaces;


import org.example.domain.Trainer;

import java.util.List;


public interface DAO_generic<T> {
    T save(T obj);

    T getById(int id);

    List<T> getAllObjects();

    boolean update(T obj, T replace);

    boolean deleteById(int id);

}
