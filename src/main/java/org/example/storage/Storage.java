package org.example.storage;

import jakarta.annotation.PostConstruct;
import org.example.domain.*;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Storage {

    @Value("${data.source}")
    String dataSource;
    private final Map<Integer, User> userMap = new HashMap<>();
    private final Map<Integer, Trainee> traineeMap = new HashMap<>();
    private final Map<Integer, Trainer> trainerMap = new HashMap<>();
    private final Map<Integer, Training> trainingMap = new HashMap<>();
    private final Map<Integer, TrainingType> trainingTypeMap = new HashMap<>();

    public Map<Integer, User> getUserMap() {
        return userMap;
    }

    public Map<Integer, Trainee> getTraineeMap() {
        return traineeMap;
    }

    public Map<Integer, Trainer> getTrainerMap() {
        return trainerMap;
    }

    public Map<Integer, Training> getTrainingMap() {
        return trainingMap;
    }

    public Map<Integer, TrainingType> getTrainingTypeMap() {
        return trainingTypeMap;
    }


    @PostConstruct
    public void initBean() {
        if (dataSource.isEmpty() | (dataSource.equals("${data.source}"))) {
            return;
        }
        File file = new File(dataSource);
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String i = sc.nextLine();
                if (i.equals("traineeStart")) {
                    int id = sc.nextInt();
                    sc.nextLine();
                    LocalDate date = LocalDate.parse(sc.nextLine());
                    String address = sc.nextLine();
                    int userId = sc.nextInt();
                    Trainee trainee = new Trainee(date, address, userId);
                    trainee.setId(id);
                    traineeMap.put(id, trainee);
                }
            }
            sc.close();

            sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String i = sc.nextLine();
                if (i.equals("userStart")) {
                    int id = sc.nextInt();
                    sc.nextLine();
                    String firstName = sc.nextLine();
                    String lastName = sc.nextLine();
                    String username = sc.nextLine();
                    String password = sc.nextLine();
                    boolean isActive = Boolean.getBoolean(sc.nextLine());
                    User user = new User(firstName, lastName, username, password, isActive);
                    user.setId(id);
                    userMap.put(id, user);
                }
            }
            sc.close();

            sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String i = sc.nextLine();
                if (i.equals("trainingTypeStart")) {
                    int id = sc.nextInt();
                    sc.nextLine();
                    String trainingTypeName = sc.nextLine();
                    TrainingType trainingType = new TrainingType(id, trainingTypeName);
                    trainingTypeMap.put(id, trainingType);
                }
            }
            sc.close();

            sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String i = sc.nextLine();
                if (i.equals("trainingStart")) {
                    int trainingId = sc.nextInt();
                    int traineeOneId = sc.nextInt();
                    int traineeTwoId = sc.nextInt();
                    int trainerId = sc.nextInt();
                    List<Integer> traineeIdList = new ArrayList<>();
                    traineeIdList.add(traineeOneId);
                    traineeIdList.add(traineeTwoId);
                    sc.nextLine();
                    String trainingTypeName = sc.nextLine();
                    int trainerTypeId = sc.nextInt();
                    sc.nextLine();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
                    LocalDateTime date = LocalDateTime.parse(sc.nextLine(), formatter);
                    int duration = sc.nextInt();
                    Training training = new Training(traineeIdList, trainerId, trainerTypeId, trainingTypeName, date, duration);
                    training.setId(trainingId);
                    trainingMap.put(trainingId, training);
                }
            }
            sc.close();

            sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String i = sc.nextLine();
                if (i.equals("trainerStart")) {
                    int trainerId = sc.nextInt();
                    int spec = sc.nextInt();
                    int userId = sc.nextInt();
                    Trainer trainer = new Trainer(spec, userId);
                    trainer.setId(trainerId);
                    trainerMap.put(trainerId, trainer);
                }
            }
            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
