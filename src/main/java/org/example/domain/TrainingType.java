package org.example.domain;

public class TrainingType {
    private int id;
    private String trainingTypeName;

    public TrainingType(int id, String trainingTypeName) {
        this.id = id;
        this.trainingTypeName = trainingTypeName;
    }

    public int getId() {
        return id;
    }

    public String getTrainingTypeName() {
        return trainingTypeName;
    }

    @Override
    public String toString() {
        return "TrainingType{" +
                "id=" + id +
                ", trainingTypeName='" + trainingTypeName + '\'' +
                '}';
    }
}

