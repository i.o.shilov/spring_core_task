package org.example.domain;

import java.time.LocalDateTime;
import java.util.List;

public class Training {

    private int id;
    private final List<Integer> traineeIdList;
    private final int trainerId;
    private final String trainingName;
    private final int trainingTypeId;

    private final LocalDateTime Date;
    private final int Duration;


    public Training(List<Integer> traineeList, int trainerId, int trainingTypeId, String trainingName, LocalDateTime date, int duration) {
        this.traineeIdList = traineeList;
        this.trainerId = trainerId;
        this.trainingName = trainingName;
        this.trainingTypeId = trainingTypeId;
        Date = date;
        Duration = duration;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public List<Integer> getTraineeIdList() {
        return traineeIdList;
    }

    public int getTrainerId() {
        return trainerId;
    }

    public String getTrainingName() {
        return trainingName;
    }

    public int getTrainingTypeId() {
        return trainingTypeId;
    }

    public LocalDateTime getDate() {
        return Date;
    }

    public int getDuration() {
        return Duration;
    }

    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", traineeList=" + traineeIdList +
                ", trainer=" + trainerId +
                ", trainingType=" + trainingTypeId +
                ", Date=" + Date +
                ", Duration=" + Duration +
                '}';
    }
}
