package org.example.domain;

public class Trainer {
    private int id;
    private int trainingTypeId;
    private final int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public int getTrainingTypeId() {
        return trainingTypeId;
    }

    public void setTrainingTypeId(int trainingTypeId) {
        this.trainingTypeId = trainingTypeId;
    }

    public Trainer(int trainingTypeId, int userId) {
        this.trainingTypeId = trainingTypeId;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "id=" + id +
                ", trainingType=" + trainingTypeId +
                ", user=" + userId +
                '}';
    }
}
