package org.example.configuration;


import org.example.DAO.DAOGenericImpl;
import org.example.domain.Trainee;
import org.example.domain.Trainer;
import org.example.domain.Training;
import org.example.domain.User;
import org.example.service.TraineeServiceImpl;
import org.example.service.TrainerServiceImpl;
import org.example.service.TrainingServiceImpl;
import org.example.service.UserServiceImpl;
import org.example.storage.Storage;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
public class AppConfiguration {

    @Bean
    @Scope("singleton")
    Storage storage() {
        return new Storage();
    }

    @Bean
    TrainerServiceImpl trainerService() {
        return new TrainerServiceImpl(DAO_trainer(), userService());
    }

    @Bean
    TraineeServiceImpl traineeService() {
        return new TraineeServiceImpl(DAO_trainee(), userService());
    }

    @Bean
    UserServiceImpl userService() {
        return new UserServiceImpl(DAO_User());
    }

    @Bean
    TrainingServiceImpl trainingService() {
        return new TrainingServiceImpl(DAO_training());
    }

    @Bean
    DAOGenericImpl<Training> DAO_training() {
        DAOGenericImpl<Training> DAO_training = new DAOGenericImpl<Training>();
        DAO_training.setObjectsMap(storage().getTrainingMap());
        return DAO_training;
    }

    @Bean
    DAOGenericImpl<Trainer> DAO_trainer() {
        DAOGenericImpl<Trainer> DAO_trainer = new DAOGenericImpl<Trainer>();
        DAO_trainer.setObjectsMap(storage().getTrainerMap());
        return DAO_trainer;
    }

    @Bean
    DAOGenericImpl<Trainee> DAO_trainee() {
        DAOGenericImpl<Trainee> DAO_trainee = new DAOGenericImpl<Trainee>();
        DAO_trainee.setObjectsMap(storage().getTraineeMap());
        return DAO_trainee;
    }

    @Bean
    DAOGenericImpl<User> DAO_User() {
        DAOGenericImpl<User> DAO_User = new DAOGenericImpl<User>();
        DAO_User.setObjectsMap(storage().getUserMap());
        return DAO_User;
    }


}
