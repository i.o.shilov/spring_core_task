package org.example;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.example.service.TraineeServiceImpl;
import org.example.service.TrainerServiceImpl;
import org.example.configuration.AppConfiguration;
import org.example.service.TrainingServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDate;

public class Main {
    public static Log log = LogFactory.getLog(Main.class);

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);

        TrainerServiceImpl trainerService = context.getBean(TrainerServiceImpl.class);
        TraineeServiceImpl traineeService = context.getBean(TraineeServiceImpl.class);
        TrainingServiceImpl trainingService = context.getBean(TrainingServiceImpl.class);

        traineeService.create("John", "Doe", LocalDate.of(1914, 7, 28), "homeaddress", true);
        traineeService.create("John", "Doe", LocalDate.of(2010, 5, 5), "homeaddress2", true);
//            traineeService.create("John","McLane", LocalDate.of(1960, 8, 3),"homeaddressMcLane",true);

        System.out.println("all trainee  list  ");
        var r = traineeService.getAllObjects();
        r.forEach(x -> System.out.println(x.toString()));

    }
}