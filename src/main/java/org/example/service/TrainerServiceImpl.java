package org.example.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.example.DAO.DAOGenericImpl;
import org.example.Main;
import org.example.domain.Trainer;
import org.example.domain.TrainingType;
import org.example.service.interfaces.PeopleService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class TrainerServiceImpl implements PeopleService<Trainer> {
    public static Log log = LogFactory.getLog(TrainerServiceImpl.class);

    private final DAOGenericImpl<Trainer> trainerDAO;
    private final UserServiceImpl userServiceImpl;

    public TrainerServiceImpl(DAOGenericImpl<Trainer> trainerDAO, UserServiceImpl userServiceImpl) {
        this.trainerDAO = trainerDAO;
        this.userServiceImpl = userServiceImpl;
    }

    public void save(Trainer trainer) {
        trainer.setId(trainerDAO.getLastId());
        trainerDAO.save(trainer);
    }

    public List<Trainer> getAllTrainers() {
        return new ArrayList<>(trainerDAO.getAllObjects());
    }

    public Trainer create(String firstname, String lastName, Boolean isActive, TrainingType trainingType) {
        Trainer trainer = new Trainer(trainingType.getId(), userServiceImpl.create(firstname, lastName, true).getId());
        save(trainer);
        log.info("Traineer ID=" + trainer.getId() + "saved");
        return trainer;
    }

    @Override
    public List<Trainer> getAllObjects() {
        return trainerDAO.getAllObjects();
    }

    @Override
    public Trainer getById(int id) {
        return trainerDAO.getById(id);
    }

}
