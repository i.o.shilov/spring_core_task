package org.example.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.example.DAO.DAOGenericImpl;
import org.example.Main;
import org.example.domain.Trainee;
import org.example.domain.User;
import org.example.service.interfaces.PeopleService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class TraineeServiceImpl implements PeopleService<Trainee> {

    public static Log log = LogFactory.getLog(TraineeServiceImpl.class);

    private DAOGenericImpl<Trainee> traineeDAO;
    private UserServiceImpl userServiceImpl;

    public TraineeServiceImpl(DAOGenericImpl<Trainee> traineeDAO, UserServiceImpl userServiceImpl) {
        this.traineeDAO = traineeDAO;
        this.userServiceImpl = userServiceImpl;
    }

    public Trainee save(Trainee trainee) {
        trainee.setId(traineeDAO.getLastId());
        return traineeDAO.save(trainee);
    }

    public Trainee create(String firstname, String lastName, LocalDate dateOfBirth, String address, Boolean isActive) {
        List<User> userList = userServiceImpl.getAllObjects();
        String username = firstname + "." + lastName;
        if (userList.stream().anyMatch(x ->
                ((x.getFirstName().equals(firstname)) & (x.getLastName().equals(lastName))))) {
            username = firstname + "." + lastName + serialGenerator();
            log.warn("THis combination already exist, created new username: =" + username);
        }
        User user = userServiceImpl.save(new User(firstname, lastName, username, passwordGenerator(), true));
        Trainee trainee = new Trainee(dateOfBirth, address, user.getId());
        save(trainee);
        log.info("Trainee ID=" + trainee.getId() + "saved");
        return trainee;
    }


    public List<Trainee> getAllObjects() {
        return new ArrayList<>(traineeDAO.getAllObjects());
    }

    public boolean update(Trainee old, Trainee replace) {
        return traineeDAO.update(old, replace);
    }

    public Trainee getById(int id) {
        return traineeDAO.getById(id);
    }


    public boolean delete(int id) {
        return traineeDAO.deleteById(id);

    }

}
