package org.example.service.interfaces;

import java.util.List;
import java.util.Random;

public interface PeopleService<T> {

    public List<T> getAllObjects();

    public T getById(int id);

    public default String passwordGenerator() {
        StringBuilder sb = new StringBuilder("");
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            char c = (char) (r.nextInt(26) + 'a');
            sb.append(c);
        }
        return sb.toString();
    }

    public default int serialGenerator() {
        return (int) ((Math.random() * (999 - 100)) + 100);
    }
}
