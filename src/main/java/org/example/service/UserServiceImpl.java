package org.example.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.example.DAO.DAOGenericImpl;
import org.example.domain.User;
import org.example.service.interfaces.PeopleService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements PeopleService<User> {

    public static Log log = LogFactory.getLog(TrainingServiceImpl.class);

    private final DAOGenericImpl<User> userDao;

    public UserServiceImpl(DAOGenericImpl<User> userDao) {
        this.userDao = userDao;
    }

    public User save(User user) {
        user.setId(userDao.getLastId());
        return userDao.save(user);
    }

    public User create(String firstname, String lastName, Boolean isActive) {
        List<User> userList = userDao.getAllObjects();
        String username = firstname + "." + lastName;
        if (userList.stream().anyMatch(x ->
                ((x.getFirstName().equals(firstname)) & (x.getLastName().equals(lastName))))) {
            username = firstname + "." + lastName + serialGenerator();
        }
        return save(new User(firstname, lastName, username, passwordGenerator(), true));
    }

    public List<User> getAllObjects() {
        return new ArrayList<>(userDao.getAllObjects());
    }

    public boolean update(User old, User replace) {
        return userDao.update(old, replace);
    }

    public User getById(int id) {
        return userDao.getById(id);
    }


    public boolean delete(int id) {
        return userDao.deleteById(id);

    }
}
