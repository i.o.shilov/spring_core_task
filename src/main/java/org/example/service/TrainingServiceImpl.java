package org.example.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.example.DAO.DAOGenericImpl;
import org.example.domain.Training;
import org.example.service.interfaces.PeopleService;

import java.time.LocalDateTime;
import java.util.List;

public class TrainingServiceImpl implements PeopleService<Training> {

    public static Log log = LogFactory.getLog(TrainingServiceImpl.class);
    private final DAOGenericImpl<Training> trainingDAO;

    public TrainingServiceImpl(DAOGenericImpl<Training> trainingDAO) {
        this.trainingDAO = trainingDAO;
    }

    public Training create(List<Integer> traineeList, int trainer, int trainingTypeId, String trainingTypeName, LocalDateTime date, int duration) {
        Training training = new Training(traineeList, trainer, trainingTypeId, trainingTypeName, date, duration);
        save(training);
        return training;
    }

    public void save(Training training) {
        training.setId(trainingDAO.getLastId());
        trainingDAO.save(training);
    }

    @Override
    public List<Training> getAllObjects() {
        return trainingDAO.getAllObjects();
    }

    @Override
    public Training getById(int id) {
        return trainingDAO.getById(id);
    }
}
