CREATE TABLE customers
(
    id         serial PRIMARY KEY,
    is_active  boolean,
    first_name VARCHAR(30),
    last_name  VARCHAR(30),
    password   VARCHAR(10),
    username   VARCHAR(30)
);

CREATE TABLE trainees
(
    id      serial PRIMARY KEY,
    dob     date,
    userid  INT unique,
    address VARCHAR(255)
),
	FOREIGN KEY (userid)
      REFERENCES customers (id));;

CREATE TABLE trainers
(
    id             serial PRIMARY KEY,
    trainingtypeid INT NOT NULL,
    userid         INT NOT NULL unique
),
	FOREIGN KEY (userid)
      REFERENCES customers (id));;

CREATE TABLE trainings
(
    id             serial PRIMARY KEY,
    duration       INT NOT NULL,
    traineeid      INT references trainees (id),
    trainerid references trainers (id),
    trainingtypeid INT NOT NULL,
    date           date,
    training_name  VARCHAR(20)
        CONSTRAINT trainings_pk PRIMARY KEY (traineeid,trainerid)
)
    );

CREATE TABLE trainingtypes
(
    id               serial PRIMARY KEY,
    trainingtypename VARCHAR(255)
);